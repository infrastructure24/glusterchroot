#! /bin/bash -x
DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") # the pkg directory that contains generateArtifacts.sh

bashlyFun(){
  if command -v bashly &>/dev/null
  then
    bashly $@
  elif command -v docker &>/dev/null
  then
    docker run --rm -it --user $(id -u):$(id -g) --volume "${DIR}/..:/app" dannyben/bashly $@
  else
    echo "ERROR neither bashly nor docker installed" >> /dev/stderr
    exit
  fi
}

rm -rf ${DIR}/artifacts
mkdir ${DIR}/artifacts

echo "DIR: $DIR" 
pushd "${DIR}/.."

# ********* BEGIN chrootdisk ***********
ls
bashlyFun --help
bashlyFun generate
VERSION=$(./chrootdisk --version)
mv "${DIR}/../chrootdisk" "${DIR}/artifacts"
# ********* END chrootdisk *************

# ******** BEGIN completions ********************
bashlyFun add completions_script
mv "${DIR}/../completions.bash" "${DIR}/artifacts"
# ******** END completions **********************

# ******** BEGIN Docs ***************************
bashlyFun render :mandoc "pkg/artifacts"
# ******** END Docs *****************************

popd