#! /bin/bash
DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")") # this directory
NAME="chrootmgr"
# alias bashly='docker run --rm -it --user $(id -u):$(id -g) --volume "$PWD:/app" dannyben/bashly'

if !command -v gem &>/dev/null; then
  echo "ERROR ruby is not installed. You can install it with:" >> /dev/stderr
  echo "sudo apt install ruby-dev" >> /dev/stderr
  exit 1
fi

if !command -v fpm &>/dev/null; then
  echo "ERROR fpm is not installed. You can install it with:" >> /dev/stderr
  echo "sudo gem install fpm" >> /dev/stderr
  exit 1
fi

pushd "${DIR}/artifacts"

VERSION=$(./chrootdisk --version)

fpm --input-type dir --output-type deb --name chrootmgr --package "$NAME-$VERSION.deb" --url "https://gitlab.com/infrastructure24/chrootdisk/" --architecture all --version "${VERSION}" --description "Creates and manages chroots" --maintainer "Michael Lundquist" --depends debootstrap --depends lvm2 --license gnu chrootdisk=/usr/bin/chrootdisk $(ls *.1 | perl -wnle 'print "$_=/usr/share/man/man1/$_"')

# --cpan
# --pre-uninstall chrootdisk chroot remove... 
# --depends 'lsblk > 2.33' (Unnecessary after issue #4)

popd