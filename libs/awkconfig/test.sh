#! /bin/bash

my_dir="$(dirname "$0")"

source "$my_dir/awkconfig.sh"

# section="sid"
# file="$my_dir/schroot.conf"
# key="descripti...   onNew"
# value="/srv/chroot/lenny.tar.gz substution"
# differentvalue="differentfile.txt"

# echo -e "\n\ntesting awkconfig_list_sections:"
# cat $file | awkconfig_list_sections

# echo -e "\n\ntesting awkconfig_get:"
# cat $file | awkconfig_get $section $key

# echo -e "\n\ntesting awkconfig_list_section_values:"
# cat $file | awkconfig_list_section_values lenny-file

# echo -e "\n\ntesting awkconfig_update_value:"
# cat $file | awkconfig_update_value "$section" "$key" "$value" > /tmp/awkconfigVals && mv -f /tmp/awkconfigVals $file
# output=$(awkconfig_get "$section" "$key" "$file")
# [output -eq]
# cat $file | awkconfig_update_value "$section" "$key" "$differentvalue" > /tmp/awkconfigVals && mv -f /tmp/awkconfigVals $file
# cat $file | awkconfig_get "$section" "$key"

# awkconfig_update_value "newSec" "newSecKey" "newSecVal" "$file" > /tmp/awkconfigVals && mv -f /tmp/awkconfigVals $file

# awkconfig_delete_section "sid" "$file" > /tmp/awkconfigVals && mv -f /tmp/awkconfigVals $file

# awkconfig_delete_value "sid-snap" "type" "$file" > /tmp/awkconfigVals && mv -f /tmp/awkconfigVals $file

# cat $file | awkconfig_add_section "sid" > /tmp/awkconfigVals && mv -f /tmp/awkconfigVals $file

# echo "my_dir: "  ${my_dir}

CHROOTNAME=chrootdisk
CHROOTDIR=/srv/chrootdisk
USER=root
FSTAB_FILE=/home/michael/Documents/gitRepos/generalK8s/chrootdisk/glusterchrootfstab
GROUP=root
TEST_CONFIG_FILE=./test.conf

awkconfig_update_value "${CHROOTNAME}" "description" "server chroot" "${TEST_CONFIG_FILE}" |
  awkconfig_update_value "${CHROOTNAME}" "directory" "${CHROOTDIR}" |
  awkconfig_update_value "${CHROOTNAME}" "root-users" "${USER}" |
  awkconfig_update_value "${CHROOTNAME}" "setup.fstab" "${FSTAB_FILE}" |
  awkconfig_update_value "${CHROOTNAME}" "root-groups" "${GROUP}" >>/tmp/schrootconfigfile && mv -f /tmp/schrootconfigfile $TEST_CONFIG_FILE