## Awk config functions. A re-write of bashly's default config functions

# escapes all regex meta characters in a string using Perl's "quotemeta" function
awkconfig_quotemeta() {
  local PERL_QUOTEMETA_SCRIPT="print quotemeta(\$ARGV[0]);"
  perl -e "${PERL_QUOTEMETA_SCRIPT}" "${1:-/dev/stdin}"
}

## Get a value from the config.
## Usage: result=$(config_get hello)
awkconfig_get() {
  local section="$(awkconfig_quotemeta $1)"
  local key="$(awkconfig_quotemeta $2)"
  #  Read KEY from [SECTION]
  # https://gist.github.com/thomedes/6201620
  # ${3:-/dev/stdin} reads from a file for param 3 or from stdin
  cat ${3:-/dev/stdin} | sed -n "/^[ \t]*\[${section}\]/,/\[/s/^[ \t]*${key}[ \t]*=[ \t]*//p"
}

awkconfig_list_sections() {
  #  List all [sections] of a .INI file
  # https://gist.github.com/thomedes/6201620
  cat ${1:-/dev/stdin} | sed -n 's/^[ \t]*\[\(.*\)\].*/\1/p'
}

awkconfig_list_section_values() {
  local section="$(awkconfig_quotemeta $1)"
  cat ${2:-/dev/stdin} | sed -n "/^[ \t]*\[${section}\]/,/\[/s/^[ \t]*\([^#; \t][^ \t=]*\).*=[ \t]*\(.*\)/\1=\2/p"
}

# adds a section if it doesn't exist yet.
awkconfig_add_section() {
  local section="$1"
  local secPat="^[[:blank:]]*$(awkconfig_quotemeta $1)[[:blank:]]*$"
  local fileContent="$(cat ${2:-/dev/stdin})"
  local numMatches=$(echo "$fileContent" | awkconfig_list_sections | grep -c $secPat)
  echo "$fileContent" >${2:-/dev/stdout}
  if [ $numMatches -eq 0 ]; then
    echo "[" "${section}" "]" >>${2:-/dev/stdout}
  fi
} # end awkconfig_add_section

awkconfig_update_value() {
  local section="$1"
  local key="$2"
  local value="$3"
  awk -v RS="(^|\n)[:blank:]*\\\\[" -v setFound=0 -v sec="$section" \
    -v kv="$key=$value" -v secPat="$(awkconfig_quotemeta "$section")" -v subPat="$(awkconfig_quotemeta "$key")" \
    '$1~"^[[:blank:]\[]*" secPat "[:blank:]*\]"{
      # note - there could be a [ at the beginning of the line...
      s = sub("[:blank:]*" subPat "[:blank:]*=[^#^;^\n]*", kv);
      $0 = $0 ((0 == s)? "\n" kv "\n" : ""); # adding kv to $0;
      secFound=1;
    }
    $1~"^[^;^#^\n^=]*\]"{
      $0 = "[" $0; # adding the record separator "[" back in
    }
    {print $0}
    END{
      if (secFound == 0){
        # adding a new section if we didnt see it yet
        print "#\n";
        print "[" sec "]\n";
        print kv;
      }
    }' "${4:-/dev/stdin}" | awk NF
  # "awk NF" removes blank lines https://stackoverflow.com/questions/10347653/awk-remove-blank-lines
  # you could also use "sed '/^[[:space:]]*$/d'" instead of "awk NF"
} # end awkconfig_update_value

awkconfig_delete_value() {
  local section=$1
  local key=$2
  awk -v RS="\n[:blank:]*\\\\[" \
    -v secPat="$(awkconfig_quotemeta "$section")" -v keyPat="$(awkconfig_quotemeta "$key")" \
    '$1~"^[:blank:]*" secPat "[:blank:]*\]"{
      s = sub("[:blank:]*" keyPat "[:blank:]*=[^#^;^\n]*", "");
      # $0 ((0 == s)? kv : "")
    }
    $1~"^[^;^#^\n^=]*\]"{
      $0 = "[" $0; # adding the record separator "[" back in
    }
    {print $0 }' "${3:-/dev/stdin}" | awk NF
  # "awk NF" removes blank lines
} # end awkconfig_delete_value

awkconfig_delete_section() {
  local section=$1
  awk -v RS="\n[:blank:]*\\\\[" \
    -v secPat="$(awkconfig_quotemeta "$section")" \
    '$1!~"^[:blank:]*" secPat "[:blank:]*\]"{ \
      if( $1~"^[^;^#^\n^=]*\]" ){\
        $0 = "[" $0; # adding the record separator "[" back in\
      }\
      print $0; \
    }\' "${2:-/dev/stdin}" | awk NF
  # "awk NF" removes blank lines
} # awkconfig_delete_section
