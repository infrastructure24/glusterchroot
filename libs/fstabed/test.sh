#!/bin/bash
my_dir="$(dirname "$0")"

source "$my_dir/fstabed.sh"

echo "running fstab_list TEST"
fstab_list '$fstab[0]=~/UUID/' 'print $fstab[0], " at ", $fstab[1]'

echo -e "\nrunning check_fstab_list TEST"
check_fstab_list '$fstab[0]=~/UUID/' 'print $fstab[0], " at ", $fstab[1]'

echo -e "\nrunning lsblk_disk_filter TEST"
lsblk_disk_filter '(ext4|xfs|vfat)' 'FSTYPE'

echo -e "\nrunning fstab_edit TEST"
# filter, edit, format, file
fstab_edit '$fstab[0]=~/UUID/' '$fstab[1]="helloworld"' 'print join(" ", @fstab)' testfstab