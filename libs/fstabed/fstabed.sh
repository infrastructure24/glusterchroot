#! /bin/bash

check_fstab_list() {
  local filters="${1:-'1'}" # maybe make this always filter for chrootdisk
  local format="${2:-'print $_'}"
  local file="${3:-/etc/fstab}"
  perl -c -Mstrict -wnl -e "/^\s*[^\s#]+/ or next; my @fstab=split(/\s+/, $&); ${filters} and ${format};" $file
}

# Internal: filters and formats lines in /etc/fstab using Perl.
# When run it: removes comments, splits fstab columns (listed below) into the $fstab array.
# fstab columns: <file system> <mount point> <type>  <options> <dump>  <pass>
# The $fstab array can then be used to filter and format the output as shown in the below example.
#
# $1 - filter (default: '1')
# $2 - format (default: 'print $_')
# $3 - file (default: '/etc/fstab')
#
# Returns lines from $3 filtered using $1, and formatted using $2
#
# Examples
#
#  fstab_list '$fstab[0]=~/UUID/' 'print $fstab[0], " at ", $fstab[1]'
#  # UUID=149294d6-cc19-441f-bfd1-ee21ed843af1 at /
#  # UUID=4CDC-4211 at /boot/efi
#
fstab_list() {
  local filters="${1:-'1'}" # maybe make this always filter for chrootdisk
  local format="${2:-'print $_'}"
  local file="${3:-/etc/fstab}"
  perl -Mstrict -wnl -e "/^\s*[^\s#]+/ or next; my @fstab=split(/\s+/, $&); ${filters} and ${format};" "$file"
}

# Internal: filters, edits and formats lines in an fstab file using Perl.
# When run it:
# - Stores the line's comment in $com, its fstab values in $val, and splits $val into the @fstab array.
# - It checks if each line matches the filter ($1) and ignores lines that don't match.
# - runs edit code in $2 (see below)
# - runs format code in $3 (see below)
#
# fstab columns: <file system> <mount point> <type>  <options> <dump>  <pass>
#
# $1 - filter (default: '1') - says which lines to edit (ignoring comments)
# $2 - edit (default: '1') - editing instructions
# $3 - format (default: 'print join(" ", @fstab), " ", $com') - how to output edited line
# $4 - file (default: '/etc/fstab') - which file we're running on
#
# Returns lines from $4 (file) filtered using $1 (filter), edited with $2 (edit), and formatted using $3 (format)
#
# Examples
#
#  fstab_edit '$fstab[0]=~/UUID/' '$fstab[1]="helloworld"' 'print join(" ", @fstab)' testfstab
#
fstab_edit() {
  local filter="${1:-'0'}"                                  # 0 - filter doesn't match
  local edit="${2:-'1'}"                                    # 1 for do nothing.
  local format="${3:-'print join(" ", @fstab), " ", $com'}" # print edits to @fstab
  local file="${4:-/etc/fstab}"
  # TODO: make this inplace
  cat "$file" | perl -Mdiagnostics -s -wnl -e '/^\s*[^\s#]+/ or (print $_ and next); # skipping lines with just comments
  my ($val) = /^([^#]*)/ ? $1 : "";                 # actual fstab values or ""
  my ($com) = /(#.*)$/ ? $1 : "";                   # Comments or ""
  our @fstab = split(/\s+/, $val);                  # Convert $val to @fstab array
  eval $filter or (print $_ and next);                # filter
  eval $edit;                                       # run the edit sequence (probably editing @fstab)
  eval $format;' -- -filter="$filter" -edit="$edit" -format="$format"
}

# Public: finds drives in /dev/sd* whose partitions all have an attribute that matches a filter.
#
# $1 - filter (default: '.*')
# $2 - attribute (default: 'MOUNTPOINT')
#
# Returns full paths to disks whose attributes match the filter
#
# Examples
#  lsblk /dev/sda -oTYPE,PATH,MOUNTPOINTS --pairs
#   #  TYPE="disk" PATH="/dev/sda" MOUNTPOINTS=""
#   #  TYPE="part" PATH="/dev/sda1" MOUNTPOINTS="/boot/efi"
#   #  TYPE="part" PATH="/dev/sda2" MOUNTPOINTS="/var/snap/firefox/common/host-hunspell\x0a/"
#  lsblk_disk_filter '(/var/snap/firefox/common/host-hunspell\\x0a/|/boot/efi)' 'MOUNTPOINTS'
#   #  /dev/sda
#
#  lsblk /dev/sda -oTYPE,PATH,FSTYPE --pairs
#   # TYPE="disk" PATH="/dev/sda" FSTYPE=""
#   # TYPE="part" PATH="/dev/sda1" FSTYPE="vfat"
#   # TYPE="part" PATH="/dev/sda2" FSTYPE="ext4"
#  lsblk_disk_filter '(ext4|xfs|vfat)' 'FSTYPE'
#   # /dev/sda
#
# REQUIRES lsblk >= 2.33 for PATH output option - https://github.com/util-linux/util-linux/blob/master/Documentation/releases/v2.33-ReleaseNotes#L372
# TODO: add support for /dev/nvme* (I don't have hardware to test this...)
lsblk_disk_filter() {
  local filter="${1:-'.*'}"
  local attribute="${2:-'MOUNTPOINT'}"

  local PERLCMD='my ($type, $path, $attr) = /^TYPE\="([^"]*)"\s*PATH\="([^"]*)"\s*$main::col\="([^"]*)"\s*$/ or next; # VAR ASSIGNMENT \
  if( $type=~/disk/i){          # CHECK IF TYPE IS DISK \
    printf("%s ",$main::disk);          # PRINTING PREVIOUS DISK \
    $main::disk=$path;                 # ASSIGN NEXT DISK \
  } elsif($attr!~/$main::filter/){    # PARTITION ATTRIBUTE MISMATCH TEST \
    $main::disk="";                   # DELETING DISK ON MISMATCH \
  } END{ printf($main::disk); } # PRINT LAST DISK'

  lsblk --pairs --noheadings --ascii -o TYPE,PATH,"$attribute" /dev/sd* | perl -s -Mstrict -wnl -E "${PERLCMD}" -- -filter="$filter" -disk='' -col=$attribute
  # perl switch example: https://stackoverflow.com/questions/31155659/how-do-i-pass-command-line-options-to-a-perl-program-with-perl-e
  # /dev/sdx describes SCSI/SATA devices, /dev/sdx\d+ describes their partitions.
  # See the /dev/ naming convention described in the "devices" section in fdisk.
} # END lsblk_disk_filter