# Chroot Disk

- This project manages mounting disks into a chroot.
  
`alias bashly='docker run --rm -it --user $(id -u):$(id -g) --volume "$PWD:/app" dannyben/bashly'`

- [bashly](https://github.com/DannyBen/bashly)

# Packaging

Chrootdisk is packaged into a .deb in the gitlab-ci.yml. Packaging also can be done manually with these commands:

```
sudo gem install fpm
cd pkg
chmod +x ./fpmpkgr.sh
chmod +x ./generateArtifacts.sh
./generateArtifacts.sh
./fpmpkgr.sh
```

# Coding style

- Comments use [tomdoc](https://github.com/mlafeldt/tomdoc.sh)
- possible additional framework: https://github.com/niieani/bash-oo-framework

# Inspiration

- FreeBSD Jails
- https://github.com/89luca89/lilipod
- https://earthly.dev/blog/chroot/
  - https://github.com/adamgordonbell/chroot-containers/

# TODO

- Add ability to work with services