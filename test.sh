#! /bin/bash
SET_CHROOT_PERL=$(
  cat <<-END
    my $cfgFile = %s;
    use Config::IniFiles;
    my $cfg = Config::IniFiles->new( -file => "./config.ini" );
    if( $cfg->SectionExists( 'chrootdisk' ) ){
      print "The value is " . $cfg->val( 'chrootdisk', 'description' ) . "."
    } else {
      $cfg->AddSection( 'chrootdisk' );
    }
    $cfg->RewriteConfig();
END
)

# SET_CHROOT_PERL=$(printf "%s\n" 'use Config::IniFiles;'\
#     'my $cfg = Config::IniFiles->new( -file => "./config.ini" );'\
#     'if( $cfg->SectionExists( "chrootdisk" ) ){'\
#     '  print "The value is " . $cfg->val( "chrootdisk", "description" ) . "."'\
#     '} else {'\
#     '  $cfg->AddSection( "chrootdisk" );'\
#     '}'\
#     '$cfg->RewriteConfig();')

printf "$SET_CHROOT_PERL" "hello"