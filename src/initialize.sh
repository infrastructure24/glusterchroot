if [[ -f $CONFIG_FILE ]]; then
  USER=$(config_get USER)
  GROUP=$(config_get GROUP)
  CHROOTNAME=$(config_get CHROOTNAME)
  CHROOTDIR=$(config_get CHROOTDIR) # /srv/chrootdisk by default
  DRY=$(config_get DRY)
  MIRROR=$(config_get MIRROR) # default - http://mirrors.edge.kernel.org/debian/
  SETUPDIR=$(config_get SETUPDIR)
fi