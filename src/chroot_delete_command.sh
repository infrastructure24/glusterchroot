if [ $DRY -eq 0 ]; then
  chrootdisk_disk_umount_command # todo add --all to this
  # removes basic stuff like mounting sys and /run that are mounted in "chroot create"
  perl -i.bak -wnl -e "s/^(\!#)*\Q${CHROOTDIR}\E.*//g" /etc/fstab # see the section on using perl as sed...
  rm -rf "${SETUPDIR}"
  rm -rf "${CHROOTDIR}"
fi

rm $CONFIG_FILE