#! /bin/bash

check_fstab_list() {
  local filters="${1:-'1'}" # maybe make this always filter for chrootdisk
  local format="${2:-'print $_'}"
  local file="${3:-/etc/fstab}"
  perl -c -Mstrict -wnl -e "/^\b[^#]+/ or next; my @fstab=split(/\s+/, $&); ${filters} and ${format};" $file
}

# Internal: filters and formats lines in /etc/fstab using Perl.
# When run it: removes comments, splits fstab columns (listed below) into the $fstab array.
# fstab columns: <file system> <mount point> <type>  <options> <dump>  <pass>
# The $fstab array can then be used to filter and format the output as shown in the below example.
#
# $1 - filter (default: '1')
# $2 - format (default: 'print $_')
# $3 - file (default: '/etc/fstab')
#
# Returns lines from /etc/fstab filtered using the filter from $1, and formatted using format from $2
#
# Examples
#
#  fstab_list '$fstab[0]=~/UUID/' 'print $fstab[0], " at ", $fstab[1]'
#  # UUID=149294d6-cc19-441f-bfd1-ee21ed843af1 at /
#  # UUID=4CDC-4211 at /boot/efi
#
fstab_list() {
  local filters="${1:-'1'}" # maybe make this always filter for chrootdisk
  local format="${2:-'print $_'}"
  file="${3:-/etc/fstab}"
  perl -Mstrict -wnl -e "/^\b[^#]+/ or next; my @fstab=split(/\s+/, $&); ${filters} and ${format};" $file
}

# TODO - adapt fstab_list to find and replace
# fstab_replace_line(){

# }

# Public: finds drives in /dev/sd* whose partitions all have an attribute that matches a filter.
#
# $1 - filter (default: '.*')
# $2 - attribute (default: 'MOUNTPOINT')
#
# Returns full paths to disks whose attributes match the filter
#
# Examples
#  lsblk /dev/sda -oTYPE,PATH,MOUNTPOINTS --pairs
#   #  TYPE="disk" PATH="/dev/sda" MOUNTPOINTS=""
#   #  TYPE="part" PATH="/dev/sda1" MOUNTPOINTS="/boot/efi"
#   #  TYPE="part" PATH="/dev/sda2" MOUNTPOINTS="/var/snap/firefox/common/host-hunspell\x0a/"
#  lsblk_disk_filter '(/var/snap/firefox/common/host-hunspell\\x0a/|/boot/efi)' 'MOUNTPOINTS'
#   #  /dev/sda
#
#  lsblk /dev/sda -oTYPE,PATH,FSTYPE --pairs
#   # TYPE="disk" PATH="/dev/sda" FSTYPE=""
#   # TYPE="part" PATH="/dev/sda1" FSTYPE="vfat"
#   # TYPE="part" PATH="/dev/sda2" FSTYPE="ext4"
#  lsblk_disk_filter '(ext4|xfs|vfat)' 'FSTYPE'
#   # /dev/sda
#
# REQUIRES lsblk >= 2.33 for PATH output option - https://github.com/util-linux/util-linux/blob/master/Documentation/releases/v2.33-ReleaseNotes#L372
# TODO: add support for /dev/nvme* (I don't have hardware to test this...)
lsblk_disk_filter() {
  local filter="${1:-'.*'}"
  local attribute="${2:-'MOUNTPOINT'}"

  local PERLCMD='my ($type, $path, $attr) = /^TYPE\="([^"]*)"\s*PATH\="([^"]*)"\s*$main::col\="([^"]*)"\s*$/ or next; # VAR ASSIGNMENT \
  if( $type=~/disk/i){          # CHECK IF TYPE IS DISK \
    printf("%s ",$main::disk);          # PRINTING LAST DISK \
    $main::disk=$path;                 # ASSIGN NEXT DISK \
  } elsif($attr!~/$main::filter/){    # PARTITION ATTRIBUTE MISMATCH TEST \
    $main::disk="";                   # DELETING DISK ON MISMATCH \
  } END{ printf($main::disk); } # PRINT LAST DISK'

  lsblk --pairs --noheadings --ascii -o TYPE,PATH,$attribute /dev/sd* | perl -s -Mstrict -wnl -E "${PERLCMD}" -- -filter="$filter" -disk='' -col=$attribute
  # perl switch example: https://stackoverflow.com/questions/31155659/how-do-i-pass-command-line-options-to-a-perl-program-with-perl-e
  # /dev/sdx describes SCSI/SATA devices, /dev/sdx\d+ describes their partitions.
  # See the /dev/ naming convention described in the "devices" section in fdisk.
} # END lsblk_disk_filter