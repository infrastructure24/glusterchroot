## [@bashly-upgrade validations]
validate_file_exists() {
  [[ -f "$1" ]] || echo "$1 does not exist."
}
