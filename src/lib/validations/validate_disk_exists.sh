## [@bashly-upgrade validations]
validate_disk_exists() {
  [[ -b "$1" ]] || echo "$1 is an invalid diskpath. Please provide a valid block device."
}