## [@bashly-upgrade validations]
validate_disk_empty() {
  if [![ -b "$1" ]]; then
    echo "$1 is an invalid diskpath. Please provide a valid block device."
  fi
  if [![ $(/sbin/sfdisk -d ${1} 2>&1) != "" ]]; then
    echo "${1} already contains a partition. Please wipe it out with \'sgdisk --zap-all $1\'"
  fi
}
