filter_chroot_exists() {
  ([[ -f "$CONFIG_FILE" ]] || echo "Chroot doesn't exist. Try calling 'chrootdisk chroot create' first.")
}
filter_no_chroot() {
  ([[ ! -f "$CONFIG_FILE" ]] || echo "Chroot already exists. Try calling 'chrootdisk chroot delete' first.")
}
filter_not_dry() {
  filter_chroot_exists && ([[ $DRY -eq 0 ]] || echo "Chroot was created with '--dry'. Delete the chroot and recreate it without '--dry'.")
}

filter_has_sudo() {
  [[ $DRY -eq 1 ]] || [[ ${args[--dry]} -eq 1 ]] || [ "$(id -u)" -eq 0 ] || echo "Permission denied. Re-run with sudo"
}

filter_lsblk_version() {
  # checks that lsblk version is at least 2.37 so it has the PATH output option
  lsblk --version | perl -wnl -e 'm:(\d+.\d+).\d+$: and $1>=2.37 or print "lsblk must be >=2.37. Your version is $1"'
}