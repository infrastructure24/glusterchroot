STATUS=${args[--status]} # [empty, mounted, unmounted, all]

DISKS=""


if [[ ! -f $CONFIG_FILE && $STATUS =~ "all" ]]; then
  # If chroot doesn't exist then only return empty and unmounted...
  STATUS="empty unmounted"
fi

if [[ $STATUS =~ "all" ]]; then
  # $STATUS contains "all"
  DISKS=$(lsblk_disk_filter "(\s*|\$${CHROOTDIR}\S*)" "MOUNTPOINTS")
else
  local partRegex=""
  if [[ $STATUS =~ "empty" ]]; then
    # $STATUS contains "empty"
    partRegex="^\b$" # "^\b$" will always fail because the empty string doesn't contain a word boundary. (Disks with any partitions will fail)
  fi
  if [[ $STATUS =~ "unmounted" ]]; then
    # $STATUS contains "unmounted"
    partRegex="$partRegex|^$" # "^$" will only match empty strings (where MOUNTPOINT is empty).
  fi
  if $(echo $STATUS | perl -e '(/\bmounted/ or exit 1) and exit 0;'); then
    # $STATUS contains "mounted"
    # note - exit 0 returns success - https://stackoverflow.com/questions/3815660/how-do-i-test-if-a-perl-command-embedded-in-a-bash-script-returns-true
    partRegex="$partRegex|^${CHROOTDIR}" # "^${CHROOTDIR}" matches disks rooted at $CHROOTDIR
  fi
  # $partRegex="(${partRegex})"
  DISKS=$(lsblk_disk_filter ${partRegex} "MOUNTPOINTS")
fi

if [[ ${args[--long]} ]]; then
  lsblk $DISKS
else
  echo $DISKS
fi