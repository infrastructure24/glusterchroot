### Error: Failed to get pool name

- [glusterfs/xlators/mgmt/glusterd/src/snapshot/glusterd-lvm-snapshot.c](https://github.com/gluster/glusterfs/blob/05b02649036407acde52eb59aa564e5f42dfaeab/xlators/mgmt/glusterd/src/snapshot/glusterd-lvm-snapshot.c#L167)

- TODO: reproduce the error outside a chroot!
- error `Failed to get pool name for device /dev/mapper/glustersdcvgrpglustersdclv` text from `cat /var/log/glusterfs/glusterd.log | grep 2022-01-22`:

<details>
  <summary>Click for full logs</summary>

```
[2023-01-22 13:05:14.418678 +0000] D [MSGID: 0] [glusterd-locks.c:657:glusterd_mgmt_v3_lock] 0-management: Lock for thor-gv0_vol successfully held by 52db9021-9c8e-49d9-a5ec-3e5f5bec30a3
[2023-01-22 13:05:14.418691 +0000] D [MSGID: 0] [glusterd-locks.c:565:glusterd_mgmt_v3_lock] 0-management: Trying to acquire lock of snap1_GMT-2023.01.22-13.05.14_snap for 52db9021-9c8e-49d9-a5ec-3e5f5bec30a3
[2023-01-22 13:05:14.418757 +0000] D [MSGID: 0] [glusterd-locks.c:657:glusterd_mgmt_v3_lock] 0-management: Lock for snap1_GMT-2023.01.22-13.05.14_snap successfully held by 52db9021-9c8e-49d9-a5ec-3e5f5bec30a3
[2023-01-22 13:05:14.418767 +0000] D [MSGID: 0] [glusterd-locks.c:512:glusterd_multiple_mgmt_v3_lock] 0-management: Returning 0
[2023-01-22 13:05:14.418809 +0000] D [rpc-clnt-ping.c:296:rpc_clnt_start_ping] 0-management: ping timeout is 0, returning
[2023-01-22 13:05:14.419433 +0000] D [MSGID: 0] [glusterd-mgmt.c:760:glusterd_mgmt_v3_initiate_lockdown] 0-management: Sent lock op req for Snapshot to 1 peers. Returning 0
[2023-01-22 13:05:14.419460 +0000] D [MSGID: 0] [glusterd-utils.c:1781:glusterd_volinfo_find] 0-management: Volume thor-gv0 found
[2023-01-22 13:05:14.419467 +0000] D [MSGID: 0] [glusterd-utils.c:1788:glusterd_volinfo_find] 0-management: Returning 0
[2023-01-22 13:05:14.419474 +0000] D [MSGID: 0] [glusterd-snapshot.c:2412:glusterd_snapshot_create_prevalidate] 0-management: snap-max-hard-limit is not present in opts dictionary
[2023-01-22 13:05:14.419547 +0000] D [run.c:242:runner_log] (-->/usr/lib/i386-linux-gnu/glusterfs/9.2/xlator/mgmt/glusterd.so(+0xc7985) [0xf3861985] -->/usr/lib/i386-linux-gnu/glusterfs/9.2/xlator/mgmt/glusterd.so(+0xc5b1c) [0xf385fb1c] -->/usr/lib/i386-linux-gnu/libglusterfs.so.0(runner_log+0xf6) [0xf7e6d9a6] ) 0-management: Get thin pool name for device /dev/mapper/glustersdcvgrpglustersdclv: lvs --noheadings -o pool_lv /dev/mapper/glustersdcvgrpglustersdclv
[2023-01-22 13:05:14.496508 +0000] E [MSGID: 106077] [glusterd-snapshot.c:1903:glusterd_is_thinp_brick] 0-management: Failed to get pool name for device /dev/mapper/glustersdcvgrpglustersdclv
[2023-01-22 13:05:14.496569 +0000] E [MSGID: 106657] [glusterd-snapshot.c:2039:glusterd_snap_create_clone_common_prevalidate] 0-management: Snapshot is supported only for thin provisioned LV. [{Ensure that all bricks of volume are thinly provisioned LV, Volume=thor-gv0}]
[2023-01-22 13:05:14.496584 +0000] E [MSGID: 106121] [glusterd-snapshot.c:2454:glusterd_snapshot_create_prevalidate] 0-management: Failed to pre validate
[2023-01-22 13:05:14.496591 +0000] E [MSGID: 106024] [glusterd-snapshot.c:2471:glusterd_snapshot_create_prevalidate] 0-management: Snapshot is supported only for thin provisioned LV. Ensure that all bricks of thor-gv0 are thinly provisioned LV.
[2023-01-22 13:05:14.496600 +0000] W [MSGID: 106029] [glusterd-snapshot.c:8628:glusterd_snapshot_prevalidate] 0-management: Snapshot create pre-validation failed
[2023-01-22 13:05:14.496606 +0000] W [MSGID: 106121] [glusterd-mgmt.c:151:gd_mgmt_v3_pre_validate_fn] 0-management: Snapshot Prevalidate Failed
[2023-01-22 13:05:14.496612 +0000] D [MSGID: 0] [glusterd-mgmt.c:244:gd_mgmt_v3_pre_validate_fn] 0-management: OP = 28. Returning -1
[2023-01-22 13:05:14.496618 +0000] E [MSGID: 106121] [glusterd-mgmt.c:1043:glusterd_mgmt_v3_pre_validate] 0-management: Pre Validation failed for operation Snapshot on local node
[2023-01-22 13:05:14.496625 +0000] E [MSGID: 106121] [glusterd-mgmt.c:2946:glusterd_mgmt_v3_initiate_snap_phases] 0-management: Pre Validation Failed
[2023-01-22 13:05:14.496730 +0000] D [rpc-clnt-ping.c:296:rpc_clnt_start_ping] 0-management: ping timeout is 0, returning
[2023-01-22 13:05:14.497060 +0000] D [MSGID: 0] [glusterd-mgmt.c:2255:glusterd_mgmt_v3_post_validate] 0-management: Sent post valaidation req for Snapshot to 1 peers. Returning 0
[2023-01-22 13:05:14.497093 +0000] D [rpc-clnt-ping.c:296:rpc_clnt_start_ping] 0-management: ping timeout is 0, returning
[2023-01-22 13:05:14.497430 +0000] D [MSGID: 0] [glusterd-mgmt.c:2444:glusterd_mgmt_v3_release_peer_locks] 0-management: Sent unlock op req for Snapshot to 1 peers. Returning 0
[2023-01-22 13:05:14.497458 +0000] D [MSGID: 0] [glusterd-locks.c:785:glusterd_mgmt_v3_unlock] 0-management: Trying to release lock of vol thor-gv0 for 52db9021-9c8e-49d9-a5ec-3e5f5bec30a3 as thor-gv0_vol
[2023-01-22 13:05:14.497476 +0000] D [MSGID: 0] [glusterd-locks.c:835:glusterd_mgmt_v3_unlock] 0-management: Lock for vol thor-gv0 successfully released
[2023-01-22 13:05:14.497488 +0000] D [MSGID: 0] [glusterd-utils.c:1781:glusterd_volinfo_find] 0-management: Volume thor-gv0 found
[2023-01-22 13:05:14.497495 +0000] D [MSGID: 0] [glusterd-utils.c:1788:glusterd_volinfo_find] 0-management: Returning 0
[2023-01-22 13:05:14.497504 +0000] D [MSGID: 0] [glusterd-locks.c:785:glusterd_mgmt_v3_unlock] 0-management: Trying to release lock of snap snap1_GMT-2023.01.22-13.05.14 for 52db9021-9c8e-49d9-a5ec-3e5f5bec30a3 as snap1_GMT-2023.01.22-13.05.14_snap
[2023-01-22 13:05:14.497514 +0000] D [MSGID: 0] [glusterd-locks.c:835:glusterd_mgmt_v3_unlock] 0-management: Lock for snap snap1_GMT-2023.01.22-13.05.14 successfully released
[2023-01-22 13:05:14.497521 +0000] D [MSGID: 0] [glusterd-utils.c:1788:glusterd_volinfo_find] 0-management: Returning -1
```
</details>

<details>
  <summary> click for output of <code>lvs</code></summary>

  ```
  root@thor:/# lvs
  LV               VG               Attr       LSize    Pool           Origin Data%  Meta%  Move Log Cpy%Sync Convert
  glustersdclv     glustersdcvgrp   Vwi-aot---    1.70t glustersdcpool        0.05
  glustersdcpool   glustersdcvgrp   twi-aot---    1.70t                       0.05   0.12
  glustersddlv     glustersddvgrp   Vwi-aot---    1.70t glustersddpool        0.05
  glustersddpool   glustersddvgrp   twi-aot---    1.70t                       0.05   0.12
  data             pve              twi-a-tz-- <338.36g                       0.00   0.50
  root             pve              -wi-ao----   96.00g
  swap             pve              -wi-ao----    8.00g
  sda-lvm-thinpool sda-lvm-thinpool twi-a-tz--   <1.79t                       0.00   0.15
  ```

</details>

<details>
<summary>click for ls of <code>/dev/mapper/</code></summary>

```
root@thor:/dev/mapper# ls
control                              glustersddvgrp-glustersddpool        pve-root
glustersdcvgrp-glustersdclv          glustersddvgrp-glustersddpool-tpool  pve-swap
glustersdcvgrp-glustersdcpool        glustersddvgrp-glustersddpool_tdata  sda--lvm--thinpool-sda--lvm--thinpool
glustersdcvgrp-glustersdcpool-tpool  glustersddvgrp-glustersddpool_tmeta  sda--lvm--thinpool-sda--lvm--thinpool_tdata
glustersdcvgrp-glustersdcpool_tdata  pve-data                             sda--lvm--thinpool-sda--lvm--thinpool_tmeta
glustersdcvgrp-glustersdcpool_tmeta  pve-data_tdata
glustersddvgrp-glustersddlv          pve-data_tmeta
```
</details>

## TODO:

- Try this command from inside the chroot
- lvs --noheadings -o pool_lv /dev/mapper/glustersdcvgrpglustersdclv