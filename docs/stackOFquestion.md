# Setup

- `/etc/schroot/chrootdisk/fstab`

```
sys /sys sysfs defaults 0 1
proc /proc proc defaults 0 0
/dev/ /dev none bind,create=file 0 0
/tmp /tmp none rw,bind 0 0
#/run/lvm /run/lvm none bind,create=file 0 0
#/run/udev /run/udev none bind,create=file 0 0
/run /run none bind,create=file 0 0
```

- `/etc/schroot/chrootdisk/copyfiles` is empty
- `/etc/schroot/chroot.d/chrootdisk.conf`

```
[chrootdisk]
description=Gluster server chroot
preserve-environment=true
setup.copyfiles=chrootdisk/copyfiles
directory=/srv/chrootdisk
root-users=root
personality=linux
setup.fstab=chrootdisk/fstab
type=directory
root-groups=root
setup.services=foo.service
```

```
$ sudo debootstrap --no-check-certificate --variant=buildd "12.0" "/srv/chrootdisk" "http://mirrors.edge.kernel.org/debian/"
```

<details>
  <summary>Click for <code>/etc/init.d/foo</code></summary>

  https://wiki.debian.org/LSBInitScripts

  ```bash
#! /bin/bash

### BEGIN INIT INFO
# Provides:          foo
# Required-Start:    $local_fs $network
# Required-Stop:     $local_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: foo service
# Description:       Run Foo service
### END INIT INFO

# Carry out specific functions when asked to by the system
case "$1" in
  start)
    echo "Starting Foo..." > /home/foo.txt
    ;;
  stop)
    echo "Stopping Foo..." > /home/foo.txt
    sleep 2
    ;;
  *)
    echo "Usage: /etc/init.d/foo {start|stop}"
    exit 1
    ;;
esac

exit 0
  ```
</details>


# Runlevel issues

- `sudo schroot --automatic-session -c chrootdisk -d / --`
- `/usr/sbin/invoke-rc.d` uses `/usr/sbin/runlevel`. `/usr/sbin/runlevel` gives the error `Running in chroot, ignoring request.` when run within a chroot.

- example: https://gist.github.com/drmalex07/298ab26c06ecf401f66c

<!-- # https://docs.gluster.org/en/main/Administrator-Guide/Start-Stop-Daemon/#systems-other-than-red-hat-and-debian



#     ~$ sudo chroot /srv/chrootdisk/ /usr/sbin/invoke-rc.d --force glusterd start
    # invoke-rc.d: could not determine current runlevel

#     chrootdisk)root@michael-Lenovo-ideapad-510-15ISK:/# service glusterd start
# Running in chroot, ignoring command 'start' 

See `man invoke-rc.d`
see `man update-rc.d`!!!!!!
-->

# SOLUTION

- The problem is solved in older versions of debian (jessie) 